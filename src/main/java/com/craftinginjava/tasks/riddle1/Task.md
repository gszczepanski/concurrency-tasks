# Riddle 1 : Async processing riddle with Futures

## Problem:

Client asks you for help with some functionality in his system. He has a legacy code for inventory processing.

This code calls remote inventory service asking for some inventory data. This is being done in Riddle1 class.
There are four Soap calls for Inventory service, which are done asynchronusly and took some time.

Then the results of the soap calls are being processed in InventoryProcessor. Each inventory process take some constant amount time.
The result of the processing is correct, however client is concerned about execution time. It takes about 30 seconds.

## Task:

Client asks you if you can help him to reduce whole operation processing time to 15 seconds. 

## Requirements:

You can change only one method in one class - InventoryProcessor produceResult().

You can modify InventoryProcessor class but without affecting contract and unpackResult() method.

Each Future<Inventory> must go through unpackResult() method in the same class.


