package com.craftinginjava.tasks.riddle1;

import org.apache.commons.lang3.time.StopWatch;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import static java.util.concurrent.TimeUnit.MILLISECONDS;
import static org.assertj.core.api.Assertions.assertThat;

public class Riddle1 {

    private static final InventoryProcessor processor = new InventoryProcessor();

    public static void main(String... args) {
        ExecutorService threadPoolForRemoteServiceCall = Executors.newFixedThreadPool(4);
        StopWatch stopWatch = new StopWatch();

        List<SoapInventoryCall> soapInventoryCalls = Arrays.asList(
                new SoapInventoryCall(1000),
                new SoapInventoryCall(3000),
                new SoapInventoryCall(10000),
                new SoapInventoryCall(3000)
        );
        List<Future<Inventory>> requestResults = new ArrayList<>();

        stopWatch.start();

        soapInventoryCalls.forEach(call -> requestResults.add(
                threadPoolForRemoteServiceCall.submit(call)
        ));

        int result = processor.produceResult(requestResults);
        stopWatch.stop();
        threadPoolForRemoteServiceCall.shutdown();

        long executionTimeInMs = stopWatch.getTime(MILLISECONDS);

        System.out.println("Execution time (ms): " + executionTimeInMs);
        System.out.println("Result value: " + result);

        assertThat(result).isEqualTo(8000);
        assertThat(executionTimeInMs).isLessThan(15000);
    }

}
