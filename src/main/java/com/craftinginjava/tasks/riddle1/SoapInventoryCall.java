package com.craftinginjava.tasks.riddle1;

import lombok.RequiredArgsConstructor;

import java.util.concurrent.Callable;

@RequiredArgsConstructor
class SoapInventoryCall implements Callable<Inventory> {

    private final long requestTimeInMs;

    public Inventory call() throws Exception {
        Thread.sleep(requestTimeInMs);
        return new Inventory(1000);
    }

}
