package com.craftinginjava.tasks.riddle1;

class InventoryDataProcessor {

    private static final long DATA_PROCESSING_TIME = 5000;

    public int process(Inventory inventory) {
        try {
            Thread.sleep(DATA_PROCESSING_TIME);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        return inventory.getSomeData() * 2;
    }

}
