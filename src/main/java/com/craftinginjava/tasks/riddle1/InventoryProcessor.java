package com.craftinginjava.tasks.riddle1;

import java.util.Collection;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

class InventoryProcessor {

    private final InventoryDataProcessor dataProcessor = new InventoryDataProcessor();

    /**
     * Method for producing result. You can not change method signature.
     */
    public int produceResult(Collection<Future<Inventory>> inventories) {
        return inventories.stream()
                            .mapToInt(this::processInventory)
                            .sum();
    }

    /**
     * This method must be called for every incoming Future<Inventory>
     *     object in inventories.
     *
     *  This method process each result.
     */
    private int processInventory(Future<Inventory> futureInventory) {
        try {
            Inventory inventory = futureInventory.get();
            return dataProcessor.process(inventory);
        } catch (ExecutionException  | InterruptedException e) {
            return 0;
        }
    }

}
