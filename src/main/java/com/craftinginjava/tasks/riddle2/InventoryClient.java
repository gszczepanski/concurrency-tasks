package com.craftinginjava.tasks.riddle2;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
class InventoryClient {

    private final InventoryService inventoryService;

    public Integer fetchQuantityForItem(String itemId) {
        return inventoryService.quantityOfItem(itemId);
    }

}
