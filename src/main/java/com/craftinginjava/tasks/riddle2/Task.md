# Riddle 2 : Problem with parallel stream 

## Problem:

Client asks you for help with some functionality in his system. 
He has small online shop and some logic, which checks item quantity in some remote storage service.
Storage service calls have various response times it can be 100 ms or 5000 ms.

One day storage service provider tells our client, that they will provide 3 instances of storage services (S1, S2, S3)
and we can do inventory call to any of them to get the fastest response.

Our client asked programmer whether he can provide some kind of router (InventoryService) which will call each service at the
same time, and then obtain response from the first finished call. Programmer did his job, took money and vanished.

However... since the router was introduced, calls started to take maximum amount of time, instead of minimum.

## Task:
Client asks you to help him with InventoryServiceRouter logic. He wants to obtain the fastest result of inventory quantity call.

## Requirements:

You can change only one method in one class - InventoryServiceRouter quantityOfItem().

Assertions in Riddle2 must be met.

