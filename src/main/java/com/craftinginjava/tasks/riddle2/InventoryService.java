package com.craftinginjava.tasks.riddle2;

interface InventoryService {

    Integer quantityOfItem(String itemId);

}
