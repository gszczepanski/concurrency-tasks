package com.craftinginjava.tasks.riddle2;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
class RemoteServerCall {

    private final String serverAddress;
    private final long execTime;

    Integer quantityForItem(String itemId) {
        System.out.println("Start Call for Server: " + serverAddress);
        try {
            Thread.sleep(execTime);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("Finished Call for Server: " + serverAddress + " with val: " + 10);
        return 10;
    }

}
