package com.craftinginjava.tasks.riddle2;

import org.apache.commons.lang3.time.StopWatch;

import static java.util.concurrent.TimeUnit.MILLISECONDS;
import static org.assertj.core.api.Assertions.assertThat;

public class Riddle2 {

    public static void main(String... args) {
        InventoryService invService = new InventoryServiceRouter();
        InventoryClient client = new InventoryClient(invService);

        StopWatch stopWatch = new StopWatch();
        stopWatch.start();

        Integer itemQty = client.fetchQuantityForItem("ITEM0008");
        stopWatch.stop();
        long executionTimeInMs = stopWatch.getTime(MILLISECONDS);

        assertThat(itemQty).isEqualTo(10);
        assertThat(executionTimeInMs).isLessThan(3200);
    }
}
