package com.craftinginjava.tasks.riddle2;

import java.util.stream.Stream;

class InventoryServiceRouter implements InventoryService {

    @Override
    public Integer quantityOfItem(String itemId) {
        return Stream.of(
                firstServerCall(),
                secondServerCall(),
                thirdServerCall()
        ).parallel()
                .map(call -> call.quantityForItem(itemId))
                .findFirst()
                .get();
    }

    private static RemoteServerCall firstServerCall() {
        return new RemoteServerCall("S1", 3000);
    }


    private static RemoteServerCall secondServerCall() {
        return new RemoteServerCall("S2", 5000);
    }


    private static RemoteServerCall thirdServerCall() {
        return new RemoteServerCall("S3", 7000);
    }

}
